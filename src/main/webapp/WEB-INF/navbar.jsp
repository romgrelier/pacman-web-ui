<nav class="navbar navbar-dark bg-dark">
    <!-- if the user is connected -->
    <c:if test="${!empty sessionScope.session_user }">
        <a href="profil" class="navbar-text"><c:out value = "${sessionScope.session_user.username }"/></a>
        <a href="signout" class="navbar-text"><c:out value = "Signout"/></a>
    </c:if>

    <!-- not connected -->
    <c:if test="${empty sessionScope.session_user }">
        <!-- connection form -->
        <form method="post" action="index" class="form-inline">
            <label for="username" class="navbar-text">Username</label>
            <input type="text" name="username" value="" size="20" maxlength="60" />

            <label for="password" class="navbar-text">Password</label>
            <input type="password" name="password" value="" size="20" maxlength="60" />

            <input type="submit"/>
        </form>

        <!-- signup page -->
        <a href="signup">Signup</a>
    </c:if>

</nav>