<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Signup</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    </head>
    <body>
	  	<nav class="navbar navbar-dark bg-dark">
		  	<!-- if the user is connected -->
		  	<c:if test="${!empty sessionScope.session_user }">
		  		<a href="profil" class="navbar-text"><c:out value = "${sessionScope.session_user.username }"/></a>
		  		<a href="signout" class="navbar-text"><c:out value = "Signout"/></a>
		 	</c:if>
		  	
		  	<!-- not connected -->
	 		<c:if test="${empty sessionScope.session_user }">
		  		<!-- connection form -->
			 	<form method="post" action="index" class="form-inline">
					<label for="username" class="navbar-text">Username</label>
					<input type="text" name="username" value="" size="20" maxlength="60" />

					<label for="password" class="navbar-text">Password</label>
					<input type="password" name="password" value="" size="20" maxlength="60" />
					
					<input type="submit"/>
			  	</form>
		  	
				<!-- signup page -->
	  			<a href="signup">Signup</a>
		  	</c:if>
		  	
	  	</nav>
	  	
	  	<!-- Signup form -->
	  	<form method="post" action="signup" class="form-group">
			<label for="username">Username</label>
			<input type="text" name="username" value="" size="20" maxlength="60" />
			<span>${errors['username']}</span>
			<br/>
			<label for="password">Password</label>
			<input type="password" name="password_first" value="" size="20" maxlength="60" />
			<span>${errors['password_first']}</span>
			<br/>
			<label for="password_second" class="navbar-text">Password</label>
			<input type="password" name="password_second" value="" size="20" maxlength="60" />
			<span>${errors['password_second']}</span>
			<br/>
			<input type="submit"/>
	  	</form>
    
    	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    </body>
</html>