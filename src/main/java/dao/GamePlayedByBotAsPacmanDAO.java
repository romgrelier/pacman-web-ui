package dao;

import beans.Bot;
import beans.Game;
import beans.GamePlayedByBotAsPacman;

import java.util.ArrayList;

public interface GamePlayedByBotAsPacmanDAO {
    ArrayList<GamePlayedByBotAsPacman> findAll();

    ArrayList<GamePlayedByBotAsPacman> findGame(Bot bot);
    ArrayList<GamePlayedByBotAsPacman> findBot(Game game);

    void create(GamePlayedByBotAsPacman gamePlayedByBotAsPacman);
    void update(GamePlayedByBotAsPacman gamePlayedByBotAsPacman);
    void delete(GamePlayedByBotAsPacman gamePlayedByBotAsPacman);
}
