package dao;

import beans.Game;
import beans.GamePlayedByPlayerAsGhost;
import beans.Player;

import java.util.ArrayList;

public interface GamePlayedByPlayerAsGhostDAO {
    ArrayList<GamePlayedByPlayerAsGhost> findAll();

    ArrayList<GamePlayedByPlayerAsGhost> findGame(Player player);
    ArrayList<GamePlayedByPlayerAsGhost> findPlayer(Game game);

    void create(GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost);
    void update(GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost);
    void delete(GamePlayedByPlayerAsGhost gamePlayedByPlayerAsGhost);
}
