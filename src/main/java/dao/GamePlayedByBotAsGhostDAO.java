package dao;

import beans.Bot;
import beans.Game;
import beans.GamePlayedByBotAsGhost;

import java.util.ArrayList;

public interface GamePlayedByBotAsGhostDAO {
    ArrayList<GamePlayedByBotAsGhost> findAll();

    ArrayList<GamePlayedByBotAsGhost> findGame(Bot bot);
    ArrayList<GamePlayedByBotAsGhost> findBot(Game game);

    void create(GamePlayedByBotAsGhost gamePlayedByBotAsGhost);
    void update(GamePlayedByBotAsGhost gamePlayedByBotAsGhost);
    void delete(GamePlayedByBotAsGhost gamePlayedByBotAsGhost);
}
