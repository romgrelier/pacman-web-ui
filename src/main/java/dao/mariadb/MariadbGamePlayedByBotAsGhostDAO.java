package dao.mariadb;

import beans.Bot;
import beans.Game;
import beans.GamePlayedByBotAsGhost;
import dao.DAOException;
import dao.GamePlayedByBotAsGhostDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MariadbGamePlayedByBotAsGhostDAO implements GamePlayedByBotAsGhostDAO {
    private MariadbDAOFactory daoFactory;

    private final String SQL_SELECT_ALL = "SELECT * FROM Joint_bot_ghost";
    private final String SQL_SELECT_BOT = "SELECT * FROM Joint_bot_ghost WHERE id_bot = ?";
    private final String SQL_SELECT_GAME = "SELECT * FROM Joint_bot_ghost WHERE id_game = ?";


    private final String SQL_INSERT = "INSERT INTO Joint_bot_ghost(id_game, id_bot, score) VALUES(?, ?, ?)";
    private final String SQL_UPDATE = "UPDATE Joint_bot_ghost SET id_game = ?, id_bot = ?, score = ? WHERE id_game = ?";
    private final String SQL_DELETE = "DELETE FROM Joint_bot_ghost WHERE id_game = ?";

    private GamePlayedByBotAsGhost map(ResultSet resultSet) throws SQLException {
        GamePlayedByBotAsGhost gamePlayedByBotAsGhost = new GamePlayedByBotAsGhost();

        gamePlayedByBotAsGhost.setBot(daoFactory.getBotDAO().find(resultSet.getLong("id_bot")));
        gamePlayedByBotAsGhost.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByBotAsGhost.setScore(resultSet.getInt("score"));

        return gamePlayedByBotAsGhost;
    }

    private GamePlayedByBotAsGhost map(ResultSet resultSet, Game game) throws SQLException {
        GamePlayedByBotAsGhost gamePlayedByBotAsGhost = new GamePlayedByBotAsGhost();

        gamePlayedByBotAsGhost.setBot(daoFactory.getBotDAO().find(resultSet.getLong("id_bot")));
        gamePlayedByBotAsGhost.setGame(game);
        gamePlayedByBotAsGhost.setScore(resultSet.getInt("score"));

        return gamePlayedByBotAsGhost;
    }

    private GamePlayedByBotAsGhost map(ResultSet resultSet, Bot bot) throws SQLException {
        GamePlayedByBotAsGhost gamePlayedByBotAsGhost = new GamePlayedByBotAsGhost();

        gamePlayedByBotAsGhost.setBot(bot);
        gamePlayedByBotAsGhost.setGame(daoFactory.getGameDAO().find(resultSet.getLong("id_game")));
        gamePlayedByBotAsGhost.setScore(resultSet.getInt("score"));

        return gamePlayedByBotAsGhost;
    }

    public MariadbGamePlayedByBotAsGhostDAO(MariadbDAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public ArrayList<GamePlayedByBotAsGhost> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByBotAsGhost> gamePlayedByBotAsGhosts = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_ALL, false);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                GamePlayedByBotAsGhost gamePlayedByBotAsGhost = map(resultSet);
                gamePlayedByBotAsGhosts.add(gamePlayedByBotAsGhost);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return gamePlayedByBotAsGhosts;
    }

    @Override
    public ArrayList<GamePlayedByBotAsGhost> findGame(Bot bot) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByBotAsGhost> GamePlayedByBotAsGhosts = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_BOT, false, bot.getId());
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                GamePlayedByBotAsGhost gamePlayedByBotAsGhost = map(resultSet, bot);
                GamePlayedByBotAsGhosts.add(gamePlayedByBotAsGhost);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return GamePlayedByBotAsGhosts;
    }

    @Override
    public ArrayList<GamePlayedByBotAsGhost> findBot(Game game) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GamePlayedByBotAsGhost> GamePlayedByBotAsGhosts = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_SELECT_GAME, false, game.getId());
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                GamePlayedByBotAsGhost gamePlayedByBotAsGhost = map(resultSet, game);
                GamePlayedByBotAsGhosts.add(gamePlayedByBotAsGhost);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }

        return GamePlayedByBotAsGhosts;
    }

    @Override
    public void create(GamePlayedByBotAsGhost gamePlayedByBotAsGhost) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_INSERT, false,
                    gamePlayedByBotAsGhost.getGame().getId(),
                    gamePlayedByBotAsGhost.getBot().getId(),
                    gamePlayedByBotAsGhost.getScore());
            int status = preparedStatement.executeUpdate();

            if(status == 0){
                throw new DAOException("Bot creation failed : no row added");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void update(GamePlayedByBotAsGhost gamePlayedByBotAsGhost) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_UPDATE, false,
                    gamePlayedByBotAsGhost.getGame().getId(),
                    gamePlayedByBotAsGhost.getBot().getId(),
                    gamePlayedByBotAsGhost.getScore(),
                    gamePlayedByBotAsGhost.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }

    @Override
    public void delete(GamePlayedByBotAsGhost gamePlayedByBotAsGhost) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = MariadbUtil.buildRequest(connection, SQL_DELETE, false, gamePlayedByBotAsGhost.getGame().getId());
            int status = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MariadbUtil.close(connection, preparedStatement);
        }
    }


}
