package dao;

import beans.Bot;

import java.util.ArrayList;

public interface BotDAO {
    Bot find(Long id);
    ArrayList<Bot> findAll();
    void create(Bot bot);
    void update(Bot bot);
    void delete(Bot bot);
}
