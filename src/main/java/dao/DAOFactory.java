package dao;

import dao.mariadb.MariadbDAOFactory;

import javax.servlet.ServletContextEvent;

public abstract class DAOFactory {
    public enum DataStorageType {
        MARIADB
    }

    public static DAOFactory getDaoFactory(DataStorageType type, ServletContextEvent sce){
        switch(type){
            case MARIADB:
                return MariadbDAOFactory.getInstance(sce);
            default:
                break;
        }
        return null;
    }

    public abstract GameDAO getGameDAO();

    public abstract MapDAO getMapDAO();

    public abstract BotDAO getBotDAO();

    public abstract PlayerDAO getPlayerDAO();

    public abstract GamePlayedByBotAsGhostDAO getGamePlayedByBotAsGhostDAO();

    public abstract GamePlayedByBotAsPacmanDAO getGamePlayedByBotAsPacmanDAO();

    public abstract GamePlayedByPlayerAsGhostDAO getGamePlayedByPlayerAsGhostDAO();

    public abstract GamePlayedByPlayerAsPacmanDAO getGamePlayedByPlayerAsPacmanDAO();
}
