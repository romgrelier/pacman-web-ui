package dao;

import beans.Game;
import beans.GamePlayedByPlayerAsPacman;
import beans.Player;

import java.util.ArrayList;

public interface GamePlayedByPlayerAsPacmanDAO {
    ArrayList<GamePlayedByPlayerAsPacman> findAll();

    ArrayList<GamePlayedByPlayerAsPacman> findGame(Player player);
    ArrayList<GamePlayedByPlayerAsPacman> findPlayer(Game game);

    void create(GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman);
    void update(GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman);
    void delete(GamePlayedByPlayerAsPacman gamePlayedByPlayerAsPacman);
}
