package dao;

import beans.Player;

import java.util.ArrayList;

public interface PlayerDAO {
    Player find(Long id);
    Player find(String name);
    ArrayList<Player> findAll();
    void create(Player player);
    void update(Player player);
    void delete(Player player);
}
