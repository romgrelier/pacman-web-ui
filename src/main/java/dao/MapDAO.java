package dao;

import beans.Map;

import java.util.ArrayList;

public interface MapDAO {
    Map find(Long id);
    ArrayList<Map> findAll();
    void create(Map map);
    void update(Map map);
    void delete(Map map);
}
