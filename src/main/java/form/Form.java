package form;

import java.util.HashMap;
import java.util.Map;

public abstract class Form {
	private String result;
	private Map<String, String> errors = new HashMap<String, String>();

	public String getResult() {
	    return result;
	}
	
	protected void setError(String field, String message) {
		errors.put(field, message);
	}

	public Map<String, String> getErreurs() {
	    return errors;
	}
}
