package form;

import beans.Player;
import dao.PlayerDAO;

import javax.servlet.http.HttpServletRequest;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ConnectionForm extends Form {
	private final String FIELD_USERNAME = "username";
	private final String FIELD_PASSWORD = "password";
	
	private final PlayerDAO playerDAO;
	
	public ConnectionForm(PlayerDAO playerDAO) {
		this.playerDAO = playerDAO;
	}
	
	private Player checkUsername(String username) throws Exception {
		if(username == null) {
			throw new Exception("No Username");
		}
		
		Player player = playerDAO.find(username);
		
		if(!player.getUsername().equals(username)) {
			throw new Exception("Unknown username");
		}
		
		return player;
	}
	
	private void checkPassword(Player player, String password) throws Exception {
		if(password == null) {
			throw new Exception("No Password");
		}
		
		try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            md.update(password.getBytes());

            byte[] bytes = md.digest();

            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < bytes.length; ++i){
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            String hash = sb.toString();
	    	
	    	if(!player.getPassword().equals(hash)) {
	    		throw new Exception("Bad password");
	    	}
	    	
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	public Player loginPlayer(HttpServletRequest request) {
		Player player = new Player();
		
		try {
			player = checkUsername(request.getParameter(FIELD_USERNAME));
		} catch (Exception e) {
			setError(FIELD_USERNAME, e.getMessage());
		}
		
		try {
			checkPassword(player, request.getParameter(FIELD_PASSWORD));
		} catch (Exception e) {
			setError(FIELD_PASSWORD, e.getMessage());
		}
    	
    	return player;
	}
}
