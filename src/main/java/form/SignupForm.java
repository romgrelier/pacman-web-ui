package form;

import beans.Player;
import dao.PlayerDAO;

import javax.servlet.http.HttpServletRequest;

public class SignupForm extends Form {
	private final String FIELD_USERNAME = "username";
	private final String FIELD_PASSWORD_FIRST = "password_first";
	private final String FIELD_PASSWORD_SECOND = "password_second";

	private final PlayerDAO playerDAO;

	public SignupForm(PlayerDAO playerDAO) {
		this.playerDAO = playerDAO;
	}

	private void checkUsername(String username) throws Exception {
		if (username == null) {
			throw new Exception("No Username");
		}
		
		Player player = playerDAO.find(username);

		if (player != null) {
			throw new Exception("already used username");
		}
	}

	private void checkPassword(String first, String second) throws Exception {
		if (!first.equals(second)) {
			throw new Exception("passwords do not match");
		}
	}

	public Player signPlayer(HttpServletRequest request) {
		Player player = new Player();

		String username = request.getParameter(FIELD_USERNAME);
		String password_first = request.getParameter(FIELD_PASSWORD_FIRST);
		String password_second = request.getParameter(FIELD_PASSWORD_SECOND);

		try {
			checkUsername(username);

			player.setUsername(username);

			try {
				checkPassword(password_first, password_second);

				player.setPassword(password_first);

				playerDAO.create(player);
			} catch (Exception e) {
				setError(FIELD_PASSWORD_FIRST, e.getMessage());
				setError(FIELD_PASSWORD_SECOND, e.getMessage());
			}

		} catch (Exception e) {
			setError(FIELD_USERNAME, e.getMessage());
		}

		return player;
	}
}
