package beans;

public class GamePlayedByBotAsGhost {
    private Bot bot;
    private Game game;
    private int score;

    public GamePlayedByBotAsGhost() {
    }

    public Bot getBot() {
        return bot;
    }

    public void setBot(Bot bot) {
        this.bot = bot;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
