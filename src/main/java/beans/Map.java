package beans;

public class Map {
    private Long id;
    private String name;
    private int pacman_nb;
    private int ghost_nb;

    public Map() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPacman_nb() {
        return pacman_nb;
    }

    public void setPacman_nb(int pacman_nb) {
        this.pacman_nb = pacman_nb;
    }

    public int getGhost_nb() {
        return ghost_nb;
    }

    public void setGhost_nb(int ghost_nb) {
        this.ghost_nb = ghost_nb;
    }
}
