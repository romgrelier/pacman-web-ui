package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/profil")
public class IndexFilter implements Filter {
    private static final String ATT_SESSION_USER = "session_user";

    private static final String PAGE_INDEX_PATH = "/index";
    private static final String PAGE_PROFIL_PATH = "/profil";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        if(httpRequest.getSession().getAttribute(ATT_SESSION_USER) == null){
            httpResponse.sendRedirect(httpRequest.getContextPath() + PAGE_INDEX_PATH);
        } else {
            httpRequest.getRequestDispatcher(PAGE_PROFIL_PATH).forward(httpRequest, httpResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
