package servlets;

import beans.Player;
import dao.*;
import form.ConnectionForm;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet implementation class ClientCreation
 */
@WebServlet("/index")
public class GameDisplayer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String JSP_PATH = "/WEB-INF/index.jsp";
	private static final String ATT_SESSION_USER = "session_user";
	
	private PlayerDAO playerDAO;
	private BotDAO botDAO;
	private GameDAO gameDAO;
	private MapDAO mapDAO;
	
	@Override
	public void init() throws ServletException {
		super.init();
    	this.playerDAO = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getPlayerDAO();
    	this.botDAO = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getBotDAO();
    	this.gameDAO = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getGameDAO();
    	this.mapDAO = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getMapDAO();
	}

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

    	request.setAttribute("bots", botDAO.findAll());
    	request.setAttribute("players", playerDAO.findAll());
    	request.setAttribute("games", gameDAO.findAll());
    	request.setAttribute("maps", mapDAO.findAll());

        this.getServletContext().getRequestDispatcher( JSP_PATH ).forward( request, response );
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {    	
    	ConnectionForm connectionForm = new ConnectionForm(playerDAO);
    	
    	Player player = connectionForm.loginPlayer(request);
    	
    	HttpSession session = request.getSession();
    	
    	if(!connectionForm.getErreurs().isEmpty()) {
    		session.setAttribute(ATT_SESSION_USER, null);
    	} else {
    		session.setAttribute(ATT_SESSION_USER, player);
    	}

		response.sendRedirect("index");
	}

}
