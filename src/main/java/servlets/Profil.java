package servlets;

import beans.GamePlayedByPlayerAsGhost;
import beans.GamePlayedByPlayerAsPacman;
import beans.Player;
import dao.DAOFactory;
import dao.GameDAO;
import dao.GamePlayedByPlayerAsGhostDAO;
import dao.GamePlayedByPlayerAsPacmanDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Servlet implementation class Profil
 */
@WebServlet("/profil")
public class Profil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String JSP_PATH = "/WEB-INF/profil.jsp";
	
	private GameDAO gameDAO;
	private GamePlayedByPlayerAsPacmanDAO gamePlayedByPlayerAsPacman;
	private GamePlayedByPlayerAsGhostDAO gamePlayedByPlayerAsGhost;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profil() {
        super();
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	this.gameDAO = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getGameDAO();
		this.gamePlayedByPlayerAsPacman = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getGamePlayedByPlayerAsPacmanDAO();
		this.gamePlayedByPlayerAsGhost = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getGamePlayedByPlayerAsGhostDAO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		
		Player player = (Player)session.getAttribute("session_user");


		ArrayList<GamePlayedByPlayerAsPacman> games_as_pacman = gamePlayedByPlayerAsPacman.findGame(player);
		ArrayList<GamePlayedByPlayerAsGhost> games_as_ghost = gamePlayedByPlayerAsGhost.findGame(player);
    	
    	request.setAttribute("games_as_pacman", games_as_pacman);
    	request.setAttribute("games_as_ghost", games_as_ghost);

        this.getServletContext().getRequestDispatcher( JSP_PATH ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
