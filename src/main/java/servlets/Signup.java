package servlets;

import beans.Player;
import dao.DAOFactory;
import dao.PlayerDAO;
import form.SignupForm;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class Signup
 */
@WebServlet("/signup")
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String JSP_PATH = "/WEB-INF/signup.jsp";
	
	private PlayerDAO playerDAO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Signup() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	this.playerDAO = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getPlayerDAO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher( JSP_PATH ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		SignupForm signupForm = new SignupForm(playerDAO);
		
		Player player = signupForm.signPlayer(request);

		if(signupForm.getErreurs().isEmpty()){
			playerDAO.create(player);
		} else {
			request.setAttribute("errors", signupForm.getErreurs());
		}

		doGet(request, response);	
	}

}
