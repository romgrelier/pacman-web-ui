-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.13-MariaDB-1:10.3.13+maria~bionic - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pacman
CREATE DATABASE IF NOT EXISTS `pacman` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pacman`;

-- Dumping structure for table pacman.Bot
CREATE TABLE IF NOT EXISTS `Bot` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Bot: ~3 rows (approximately)
DELETE FROM `Bot`;
/*!40000 ALTER TABLE `Bot` DISABLE KEYS */;
INSERT INTO `Bot` (`id`, `name`) VALUES
	(1, '2B'),
	(2, '9S'),
	(3, 'A2');
/*!40000 ALTER TABLE `Bot` ENABLE KEYS */;

-- Dumping structure for table pacman.Game
CREATE TABLE IF NOT EXISTS `Game` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `id_map` int(10) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `id_map` (`id_map`),
  CONSTRAINT `id_map` FOREIGN KEY (`id_map`) REFERENCES `Map` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Game: ~0 rows (approximately)
DELETE FROM `Game`;
/*!40000 ALTER TABLE `Game` DISABLE KEYS */;
/*!40000 ALTER TABLE `Game` ENABLE KEYS */;

-- Dumping structure for table pacman.Joint_bot_ghost
CREATE TABLE IF NOT EXISTS `Joint_bot_ghost` (
  `id_game` int(10) unsigned NOT NULL,
  `id_bot` int(10) unsigned NOT NULL,
  `score` int(10) unsigned NOT NULL,
  KEY `id_bot_ghost_game` (`id_game`),
  KEY `id_bot_ghost_bot` (`id_bot`),
  CONSTRAINT `id_bot_ghost_bot` FOREIGN KEY (`id_bot`) REFERENCES `Bot` (`id`),
  CONSTRAINT `id_bot_ghost_game` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Joint_bot_ghost: ~0 rows (approximately)
DELETE FROM `Joint_bot_ghost`;
/*!40000 ALTER TABLE `Joint_bot_ghost` DISABLE KEYS */;
/*!40000 ALTER TABLE `Joint_bot_ghost` ENABLE KEYS */;

-- Dumping structure for table pacman.Joint_bot_pacman
CREATE TABLE IF NOT EXISTS `Joint_bot_pacman` (
  `id_game` int(10) unsigned NOT NULL,
  `id_bot` int(10) unsigned NOT NULL,
  `score` int(10) unsigned NOT NULL,
  KEY `id_bot_pacman_game` (`id_game`),
  KEY `id_bot_pacman_bot` (`id_bot`),
  CONSTRAINT `id_bot_pacman_bot` FOREIGN KEY (`id_bot`) REFERENCES `Bot` (`id`),
  CONSTRAINT `id_bot_pacman_game` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Joint_bot_pacman: ~0 rows (approximately)
DELETE FROM `Joint_bot_pacman`;
/*!40000 ALTER TABLE `Joint_bot_pacman` DISABLE KEYS */;
/*!40000 ALTER TABLE `Joint_bot_pacman` ENABLE KEYS */;

-- Dumping structure for table pacman.Joint_player_ghost
CREATE TABLE IF NOT EXISTS `Joint_player_ghost` (
  `id_game` int(10) unsigned NOT NULL,
  `id_player` int(10) unsigned NOT NULL,
  `score` int(10) unsigned NOT NULL,
  KEY `id_player_ghost_game` (`id_game`),
  KEY `id_player_ghost_player` (`id_player`),
  CONSTRAINT `id_player_ghost_game` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id`),
  CONSTRAINT `id_player_ghost_player` FOREIGN KEY (`id_player`) REFERENCES `Player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Joint_player_ghost: ~0 rows (approximately)
DELETE FROM `Joint_player_ghost`;
/*!40000 ALTER TABLE `Joint_player_ghost` DISABLE KEYS */;
/*!40000 ALTER TABLE `Joint_player_ghost` ENABLE KEYS */;

-- Dumping structure for table pacman.Joint_player_pacman
CREATE TABLE IF NOT EXISTS `Joint_player_pacman` (
  `id_game` int(10) unsigned NOT NULL,
  `id_player` int(10) unsigned NOT NULL,
  `score` int(10) unsigned NOT NULL,
  KEY `id_player_pacman_game` (`id_game`),
  KEY `id_player_pacman_player` (`id_player`),
  CONSTRAINT `id_player_pacman_game` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id`),
  CONSTRAINT `id_player_pacman_player` FOREIGN KEY (`id_player`) REFERENCES `Player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Joint_player_pacman: ~0 rows (approximately)
DELETE FROM `Joint_player_pacman`;
/*!40000 ALTER TABLE `Joint_player_pacman` DISABLE KEYS */;
/*!40000 ALTER TABLE `Joint_player_pacman` ENABLE KEYS */;

-- Dumping structure for table pacman.Map
CREATE TABLE IF NOT EXISTS `Map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `pacman_count` int(10) unsigned NOT NULL,
  `ghost_count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Map: ~6 rows (approximately)
DELETE FROM `Map`;
/*!40000 ALTER TABLE `Map` DISABLE KEYS */;
INSERT INTO `Map` (`id`, `name`, `pacman_count`, `ghost_count`) VALUES
	(1, 'bigCorners.lay', 1, 0),
	(2, 'bigMaze.lay', 1, 0),
	(3, 'bigSafeSearch.lay', 1, 1),
	(4, 'bigSearch.lay', 1, 0),
	(5, 'bigSearch_onePacman_oneGhost.lay', 1, 1),
	(6, 'bigSearch_twoPacmans.lay', 2, 0),
	(7, 'bigSearch_twoPacmans_oneGhost.lay', 2, 1),
	(8, 'bigSearch_twoPacmans.lay', 2, 0),
	(9, 'bigSearch.lay', 1, 0),
	(10, 'boxSearch.lay', 1, 2),
	(11, 'capsuleClassic.lay', 1, 3),
	(12, 'contestClassic.lay', 1, 3),
	(13, 'contoursMaze.lay', 1, 0),
	(14, 'greedySearch.lay', 1, 0),
	(15, 'mediumClassic_fivePacmans.lay', 5, 4),
	(16, 'mediumClassic_manyPacmans.lay', 14, 9),
	(17, 'mediumClassic_onePacman.lay', 1, 2),
	(18, 'mediumClassic_twoPacmans.lay', 2, 2),
	(19, 'mediumClassic.lay', 2, 2),
	(20, 'mediumCorners.lay', 1, 0),
	(21, 'mediumDottedMaze.lay', 1, 0),
	(22, 'mediumMaze.lay', 1, 0),
	(23, 'mediumSafeSearch.lay', 1, 1),
	(24, 'mediumScaryMaze.lay', 1, 4),
	(25, 'mediumSearch.lay', 1, 0),
	(26, 'mediumSearch.lay', 1, 0),
	(27, 'minimaxClassic.lay', 1, 3),
	(28, 'oddSearch.lay', 1, 0),
	(29, 'openClassic.lay', 1, 1),
	(30, 'openMaze.lay', 1, 0),
	(31, 'openSearch.lay', 1, 0),
	(32, 'originalClassic_oneFood_fivePacmans.lay', 5, 4),
	(33, 'originalClassic_oneFood_TenPacmans.lay', 10, 4),
	(34, 'originalClassic_oneFood_twoPacmans.lay', 2, 6),
	(35, 'originalClassic_oneFood_twoPacmans.lay', 1, 6),
	(36, 'originalClassic.lay', 1, 4),
	(37, 'smallClassic.lay', 1, 2),
	(38, 'smallMaze.lay', 1, 0),
	(39, 'smallSafeSearch.lay', 1, 1),
	(40, 'smallSearch.lay', 1, 0),
	(41, 'testClassic.lay', 1, 1),
	(42, 'testMaze.lay', 1, 0),
	(43, 'testSearch.lay', 1, 0),
	(44, 'tinyCorners.lay', 1, 0),
	(45, 'tinyMaze.lay', 1, 0),
	(46, 'tinySafeSearch.lay', 1, 1),
	(47, 'tinySearch.lay', 1, 0),
	(48, 'trappedClassic.lay', 1, 2),
	(49, 'trickyClassic.lay', 1, 4),
	(50, 'trickySearch.lay', 1, 0);
/*!40000 ALTER TABLE `Map` ENABLE KEYS */;

-- Dumping structure for table pacman.Player
CREATE TABLE IF NOT EXISTS `Player` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table pacman.Player: ~41 rows (approximately)
DELETE FROM `Player`;
/*!40000 ALTER TABLE `Player` DISABLE KEYS */;
INSERT INTO `Player` (`id`, `username`, `password`) VALUES
	(13, 'James', 'ce08becc73195df12d99d761bfbba68d'),
	(14, 'Jai', '568628e0d993b1973adc718237da6e93'),
	(15, 'Li', 'a13ee062eff9d7295bfc800a11f33704'),
	(16, 'Naomi', 'ea20a043c08f5168d4409ff4144f32e2'),
	(17, 'Leon', '84eb13cfed01764d9c401219faa56d53'),
	(18, 'Daisy', '8fc4c7ab4453d247e011738197b6136c'),
	(19, 'Robert', 'c4de9fe96832a877668d0dced80657b8'),
	(20, 'Joshua', '5c7d5705ab73466c0584cc782cb12d2e'),
	(21, 'Vinh', '9f9cee5b98b2e350d739b5ebea4576a6'),
	(22, 'Otoo', '28fd0fbd334515deb8a8291b71941c9e'),
	(23, 'Samuel', 'e1eee5e2b42d45443cdc82db1a3bc465'),
	(24, 'Randall', 'b6aea7af56564f32a22ce27f25936b82'),
	(25, 'Isaac', '2a452c50d7c60fb1974ac813f3c6f2bb'),
	(26, 'Douglas', '2ba100d60448c3b5fe2dc55a892104f0'),
	(27, 'William', '2f1ee6251bc23a0f35118b54170177cb'),
	(28, 'Anton', 'bff82d18862ce94df14bdee55295f812'),
	(29, 'Keiichii', '2c4a8495407bfb55ce1a93970445bd38'),
	(30, 'Kurt', '8c9611718f0422ac8dd0baf0903d22e5'),
	(31, 'Jorge', 'a71d83915f8839e1da511021db1b3aa0'),
	(32, 'Margaret', '5759d9ff2a54eeab381f9cf6062800e8'),
	(33, 'Linda', 'e7db56dbec713f0510010c6d997d9ddd'),
	(34, 'Malcolm', '682670d9ab0aaa4dc01895a59aa8f91b'),
	(35, 'Maria', '18230e1fb6e5dd3cfb0bcec4f863e167'),
	(36, 'Sheila', 'a91f7d0f27f3c35bd2692c05876cc13f'),
	(37, 'Solomon', '275a967a527ef7d3279ac140323c569a'),
	(38, 'Arthur', '38b18881fc8d864a177afe3864c10aba'),
	(39, 'Kelly', '962e611a0fdbd09ad6368caece8df75c'),
	(40, 'Jerome', 'e64770ac6bac05626b4f59b6a03e2ff8'),
	(41, 'Grace', 'f43686bab1c60489c1eac5ecb1ec8c7b'),
	(42, 'August', '8638096e4ddb49a0dd6592c57d9f50ab'),
	(43, 'Victor', '38b3eff8baf56627478ec76a704e9b52'),
	(44, 'Frederic', 'c9e1074f5b3f9fc8ea15d152add07294'),
	(45, 'Adriana', '698d51a19d8a121ce581499d7b701668'),
	(46, 'John', 'eb160de1de89d9058fcb0b968dbbbd68'),
	(47, 'Mike', 'da4fb5c6e93e74d3df8527599fa62642'),
	(48, 'Joseph', 'a0a080f42e6f13b3a2df133f073095dd'),
	(49, 'Oscar', 'd1f491a404d6854880943e5c3cd9ca25'),
	(50, 'Alice', '9b8619251a19057cff70779273e95aa6'),
	(51, 'Carris', '3988c7f88ebcb58c6ce932b957b6f332'),
	(52, 'Cal', '0f28b5d49b3020afeecd95b4009adf4c'),
	(53, 'Roma', '903ce9225fca3e988c2af215d4e544d3');
/*!40000 ALTER TABLE `Player` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
