FROM tomcat:9.0.16-jre11

ADD build/libs/pacman_web.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]
